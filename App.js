/*
1) Функції потрібні для того, щоб виконувати певний іменований шматок коду декілька разів.
2) Аргумент ми передаємо для того, щоб використовувати його всередині функції, виконувати з ним різні операції. Але слід зазаначити, що аргумент у функцію передавати не обов'язково.
3) Оператор Return явно повертає нам кінцевий результат з функції. Після нього, якщо є написаний код, він виконуватись не буде. Без Return функція за замовчуванням повертає undefined.
*/

// 

const calcNumbers = (num1, num2, calc) => {

  num1 = +prompt('Введіть число 1')
  while (isNaN(num1) || num1 == false) {
    num1 = +prompt('Введіть ще раз число 1') 
  }

  num2 = +prompt('Введіть число 2')
  while (isNaN(num2) || num2 == false) {
    num2 = +prompt('Введіть ще раз число 2') 
  }

  calc = prompt('Введіть операцію "+", "-", "*", "/"')
  let result

  if (calc === '+') {
    result = num1 + num2
    return result
  } else if (calc === '-') {
    result = num1 - num2
    return result
  } else if (calc === '*') {
    result = num1 * num2
    return result
  } else if (calc === '/') {
    result = num1 / num2
    return result
  }

}

console.log(calcNumbers())